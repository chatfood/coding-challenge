# Fizzbuzz task

- "Inputs" are numbers from 1 to 100
- Every number divided by 3 is Fizz
- Every number divided by 5 is Buzz
- Every number divided by 3 and 5 is FizzBuzz
- Return an array of the special values above or the current index

## Setup
- Install dependencies `flutter pub get`

## Scripts
- Run tests `flutter test`