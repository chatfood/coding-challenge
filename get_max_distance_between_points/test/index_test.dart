// Import the test package and Counter class
import 'package:test/test.dart';
import 'package:get_max_distance_between_points/index.dart';

void main() {
  test('GetLongestDistanceBetweenPoints should work', () {
    expect(getLongestDistanceBetweenPoints(), -1);
  });
}
