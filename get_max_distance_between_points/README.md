# Get longest distance between coordinate points

## Setup
- Install dependencies `flutter pub get`

## Scripts
- Run tests `flutter test`

## Requirements
You have an XY coordinate system with points on it. You should implement the tests and logic for `getLongestDistanceBetweenPoints` function.

- N points; N >= 0
- Function returns the largest distance between all points
- Function returns a `number`
- Cover functionality with unit tests



