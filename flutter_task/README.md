# Restaurant menu
Our company needs to display the menu of one of our restaurants.

<img src="https://chatfood-cdn.s3.eu-central-1.amazonaws.com/fe-code-challenge-1/menu.png" style="width: 200px;" />

---

## Requirements:
Create a mobile app that will display the restaurant's menu with the following features
- The user should be able to filter the items by name;
- Clicking in the item line will add it to the basket and if it's already there, will increase the quantity by 1 until it reaches the stock availability;
- Should display the discounted price when available;
- Cover existing codebase with unit tests.

## Assets
The layout has to follow the provided mockup:<br>
https://www.figma.com/file/DpaOItWTwhkaeyOJrJAMyL/Coding-Challenge?node-id=1%3A86

Use the endpoint below to fetch the menu<br>
https://chatfood-cdn.s3.eu-central-1.amazonaws.com/fe-code-challenge-1/menu.json

## Notes
- Focus on business logic. Styles are secondary.
- You don't have to finish 100% of the requirements.
- We value attention to details. Say this to the interviewer "It's not rocket science"
- Googling is allowed
- Pair programming (with minimal help from us) is allowed
